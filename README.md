# Jared's build of slock

![Screenshot of my slock](https://gitlab.com/jaredreardon/slock/raw/master/slock_screenshot.png)

slock is one of the tools created by the [suckless](https://suckless.org) guys. This is my personal build of slock, which I added a couple patches to:
+ capscolor: adds a separate color for if your Caps Lock key is on when typing in your password
+ dwmlogo: adds a cool dwm logo to the center of slock, which becomes the thing that changes color when you enter your password or get the password wrong, rather than the whole screen changing color
+ Xresources: allow Xresources file to define the colors for slock, to more easily integrate slock into the general desktop aesthetic

# Installation

Download the source code from this repository or use a git clone:

	git clone https://gitlab.com/jaredreardon/slock.git
	cd slock
	sudo make clean install

NOTE: Installing my slock build will overwrite your existing slock installation!

# Running slock

slock can an easily be implemented into a dmenu script, but if you just want to test it out first just run this command in the terminal:

    slock

# Configuring slock

The configuration of slock, just like the other suckless utilities like dwm and dmenu, is done by editng the config.h file and (re)compiling the source code. The source code will be located where you git cloned it but in case you want to copy me, I keep mine in ~/.local/src. Type the following command to recompile slock:

	sudo make clean install
